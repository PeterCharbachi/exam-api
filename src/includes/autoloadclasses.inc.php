<?php
// peter //Funktion för att ladda rätt class.
function autoClasses($classname)
{

    // peter //om inte classfilen finns så returnera false
    if (!is_file('api/' . $classname . '.php')) {
        return false;
    }
    // peter //annars inkludera filen:
    require_once 'api/' . $classname . '.php';
}
