<?php

class DB
{

    private $pdo;

    private $dbUser = "root";
    private $dbPass = "root";
    private $dbHost = "localhost";
    private $dbName = "examensarbete";

    private function __construct() {

        $connectionString = 'mysql:dbname='.$this->dbName.';host='.$this->dbHost;
        try {
            $this->pdo = new PDO($connectionString, $this->dbUser, $this->dbPass);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection failed: '.$e->getMessage();
        }
    }

    // För singleton....
    private static $instance;
    public static function getInstance()
    {
        if (!self::$instance instanceof DB) {
            // Om statiska egenskapen $instance INTE är ett objekt av DB - skapa ett objekt av DB.
            self::$instance = new DB();
        }
        return self::$instance;
    }

    public static function getConnection()
    {
        return DB::getInstance()->pdo;
    }

}
