<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Hämtar alla produkter
$app->get('/api/products', function(Request $request, Response $response) {
    $sql = "SELECT * FROM products";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// söker efter produkt
$app->get('/api/products/search/{name}', function(Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $sql = "SELECT * FROM products WHERE product_name LIKE '%$name%' OR product_description LIKE '%$name%'";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar specfik produkt
$app->get('/api/products/{pid}', function(Request $request, Response $response) {
    $pid = $request->getAttribute('pid');
    $sql = "SELECT * FROM products WHERE product_id = $pid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar alla produkter som tillhär specfik supplier
$app->get('/api/suppliers/{sid}/products', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM products WHERE products.supplier_id = $sid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar specfik produkt som tillhär supplier
$app->get('/api/suppliers/{sid}/products/{pid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $pid = $request->getAttribute('pid');
    $sql = "SELECT * FROM products WHERE products.supplier_id = $sid and products.product_id = $pid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar alla produkter som tillhör specifik produkt
$app->get('/api/categories/{cid}/products', function(Request $request, Response $response) {
    $cid = $request->getAttribute('cid');
    $sql = "SELECT * FROM products where products.supplier_category_id = $cid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

$app->get('/api/store/categories/{cid}/products', function(Request $request, Response $response) {
    $cid = $request->getAttribute('cid');
    $sql = "SELECT * FROM products where products.category_id = $cid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar reviews från specfik produkt
$app->get('/api/customer/reviews/product/{pid}', function(Request $request, Response $response) {
    $pid = $request->getAttribute('pid');
    $sql = "SELECT first_name, reviews_text, date FROM reviews, customers WHERE product_id = :pid and reviews.customer_id = customers.customer_id";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':pid', $pid);
    $data = $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// skapar reviews för spefik produkt
$app->post('/api/customer/reviews/add', function(Request $request, Response $response) {
    intval($customer_id = $request->getParam('customer_id'));
    intval($product_id = $request->getParam('product_id'));
    ucfirst(strtolower(filter_var($reviewsText = $request->getParam('reviewsText'))));
    $sql = "INSERT INTO reviews (product_id, customer_id, reviews_text) VALUES (:product_id, :customer_id, :reviewsText)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':customer_id', $customer_id);
    $stmt->bindParam(':product_id', $product_id);
    $stmt->bindParam(':reviewsText', $reviewsText);
    $data = $stmt->execute();

    echo json_encode($data);
});

// uppdaterar produkt
$app->post('/api/supplier/products/put', function(Request $request, Response $response) {
    intval($productId = $request->getParam('product_id'));
    ucfirst(strtolower(filter_var($product_name = $request->getParam('product_name'))));
    ucfirst(strtolower(filter_var($product_description = $request->getParam('product_description'))));
    intval($category_id = $request->getParam('category_id'));
    intval($supplier_category_id = $request->getParam('supplier_category_id'));
    intval($price = $request->getParam('price'));
    ucfirst(strtolower(filter_var($sku = $request->getParam('sku'))));

    $files = $request->getUploadedFiles();
    if(!empty($_FILES)) {
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 200000){
                $fileNameNew = uniqid(true). ".". $fileActualExt;
                $fileDestination = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\webbshop\src\assets\images/'. $fileNameNew;
                $fileDestination2 = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\suppliers-admin\src\assets\images/'. $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                copy($fileDestination, $fileDestination2);
            } else{
                die("You're file is to big!");
            }
        }else{
            die("There was an error uploading your file!");
        }
    }else{
        die("You cannot upload files of this type!");
    }
    }
    $sql = "UPDATE products SET sku = :sku, product_name = :productName, product_description = :productDescription, supplier_category_id = :supplierCategoryId, category_id = :cid, price = :price, picture = :picture WHERE (product_id = :pid);";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':pid', $productId);
    $stmt->bindParam(':sku', $sku);
    $stmt->bindParam(':productName', $product_name);
    $stmt->bindParam(':productDescription', $product_description);
    $stmt->bindParam(':supplierCategoryId', $supplier_category_id);
    $stmt->bindParam(':cid', $category_id);
    $stmt->bindParam(':price', $price);
    $stmt->bindParam(':picture', $fileNameNew);
    $data = $stmt->execute();

    echo json_encode($data);
});

// skapar produkt 
$app->post('/api/suppliers/products/add', function(Request $request, Response $response) {
    intval($sid = $request->getParam('supplier_id'));
    ucfirst(strtolower(filter_var($product_name = $request->getParam('product_name'))));
    ucfirst(strtolower(filter_var($product_description = $request->getParam('product_description'))));
    intval($supplier_category_id = $request->getParam('supplier_category_id'));
    intval($category_id = $request->getParam('category_id'));
    intval($price = $request->getParam('price'));
    ucfirst(strtolower(filter_var($sku = $request->getParam('sku'))));
    // om man inte har valt att lägga till categorier så ska det sättas null i databasen
    if ($supplier_category_id === '0' ) {
        $supplier_category_id = NULL;
    }
    if( $category_id === '0') {
        $category_id = NULL;
    }
    // var_dump($_FILES);
    // $files = $request->getUploadedFiles();
    if(!empty($_FILES)) {
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));


    $allowed = array('jpg', 'jpeg', 'png');
    if (!empty($fileName)) {
        if (in_array($fileActualExt, $allowed)) {
            if ($fileError === 0) {
                if ($fileSize < 200000){
                    $fileNameNew = uniqid(true). ".". $fileActualExt;
                    $fileDestination = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\webbshop\src\assets\images/'. $fileNameNew;
                    $fileDestination2 = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\suppliers-admin\src\assets\images/'. $fileNameNew;
                    move_uploaded_file($fileTmpName, $fileDestination);
                    copy($fileDestination, $fileDestination2);
                } else{
                    die("You're file is to big!");
                }
            }else{
                die("There was an error uploading your file!");
            }
        }else{
            die("You cannot upload files of this type!");
        }
    }

    }


    $sql = "INSERT INTO products (sku, product_name, product_description, supplier_category_id, category_id, supplier_id, price, picture) VALUES (:sku, :productName, :productDescription, :supplierCategoryId, :categoryId, :sid, :price, :picture)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':productName', $product_name);
    $stmt->bindParam(':productDescription', $product_description);
    $stmt->bindParam(':supplierCategoryId', $supplier_category_id);
    $stmt->bindParam(':categoryId', $category_id);
    $stmt->bindParam(':price', $price);
    $stmt->bindParam(':sku', $sku);
    $stmt->bindParam(':picture', $fileNameNew);
    $data = $stmt->execute();

 

    echo json_encode($data);
});

// ta bort produkt
$app->delete('/api/suppliers/products/{pid}/delete', function(Request $request, Response $response) {
    $pid = $request->getAttribute('pid');
    $sql = "DELETE FROM products WHERE (product_id = $pid)";
    $stmt = DB::getConnection()->query($sql);
});