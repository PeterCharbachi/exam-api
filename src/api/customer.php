<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// secret skickas in och kunden får sin data.
$app->get('/api/customers/{secret}', function(Request $request, Response $response) {
    $sql = "SELECT * FROM customers";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// uppdaterar kundens uppgidet
$app->put('/api/customer/put', function(Request $request, Response $response) {
    intval($cid = $request->getParam('customer_id'));
    ucfirst(strtolower(filter_var($firstName = $request->getParam('first_name'))));
    ucfirst(strtolower(filter_var($lastName = $request->getParam('last_name'))));
    ucfirst(strtolower(filter_var($address1 = $request->getParam('address1'))));
    ucfirst(strtolower(filter_var($address2 = $request->getParam('address2'))));
    trim($phone = $request->getParam('phone'));
    strtolower(filter_var($email = $request->getParam('email')));

    $sql = "UPDATE customers SET first_name = :firstName, last_name = :lastName, address1 = :address1, address2 = :address2, phone = :phone, email = :email WHERE (customer_id = :cid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':cid', $cid);
    $stmt->bindParam(':firstName', $firstName);
    $stmt->bindParam(':lastName', $lastName);
    $stmt->bindParam(':address1', $address1);
    $stmt->bindParam(':address2', $address2);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':email', $email);
    $data = $stmt->execute();

    echo json_encode($data);
});