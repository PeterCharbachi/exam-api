<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// skapar order
$app->post('/api/orders/add', function(Request $request, Response $response) {
    intval($cid = $request->getParam('customer_id'));
    ucfirst(strtolower(filter_var($first_name = $request->getParam('first_name'))));
    ucfirst(strtolower(filter_var($last_name = $request->getParam('last_name'))));
    strtolower(filter_var($email = $request->getParam('email')));
    trim($phone = $request->getParam('phone'));
    ucfirst(strtolower(filter_var($address1 = $request->getParam('address1'))));
    intval($orderSum = $request->getParam('order_sum'));
    
    $sql = "INSERT INTO ordes (cid, delivery_address, first_name, last_name, email, phone_number, order_sum) VALUES (:customer_id, :address1, :first_name, :last_name, :email, :phone, :orderSum)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':first_name', $first_name);
    $stmt->bindParam(':address1', $address1);
    $stmt->bindParam(':last_name', $last_name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':orderSum', $orderSum);
    $stmt->bindParam(':customer_id', $cid);
    $data = $stmt->execute();

    $newCartId = DB::getConnection()->lastInsertId();
    $pids = $request->getParam('products');

    // här loppas varje produkt och sparas i databasen
    for ($i=0; $i < count($pids); $i++) {
        $product = $pids[$i];
        $pid = $product['product_id'];
        $amount = $product['count'];
        $amount = $amount . "";
        $price = $product['price'];
        $sid = $product['supplier_id'];
        $sql = "INSERT INTO order_products (oid, pid, amount, price, supplier_id) VALUES (:oid, :pid, :amount, :price, :sid)";
        $stmt = DB::getConnection()->prepare($sql);
        $stmt->bindParam(':oid', $newCartId);
        $stmt->bindParam(':pid', $pid);
        $stmt->bindParam(':amount', $amount);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':sid', $sid);
        $data = $stmt->execute();
    }

    echo json_encode($data);
});

// hämtar alla produkter som tillhär en specfik order
$app->get('/api/supplier/{sid}/order/{oid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $oid = $request->getAttribute('oid');
    $sql = "SELECT * FROM order_products WHERE oid = $oid and supplier_id = $sid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar alla ordar som  tillhär en spefik supplier
$app->get('/api/supplier/order/{sid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT ordes.oid, ordes.first_name, ordes.last_name, ordes.email, ordes.delivery_address, ordes.payment_method, ordes.phone_number, ordes.status FROM ordes, order_products where ordes.oid = order_products.oid and order_products.supplier_id = $sid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});