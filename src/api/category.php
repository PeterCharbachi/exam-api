<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


// hämtar alla kategorier från supplierns id
$app->get('/api/supplier/categories/{sid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM supplier_category WHERE supplier_id = $sid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar specfik kategori från suppliens id och kategoriens id
$app->get('/api/supplier/{sid}/categories/{cid}', function(Request $request, Response $response) {
    $cid = $request->getAttribute('cid');
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM supplier_category WHERE supplier_id = :sid and category_id = :cid";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':cid', $cid);
    $data = $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar alla kategorier
$app->get('/api/categories', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM category";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar alla subkategorier från supplier som inte har en kategori id
$app->get('/api/supplier/{sid}/subcategories', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM supplier_subcategory where supplier_id = :supplier_id AND category_id IS NULL";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':supplier_id', $sid);
    $data = $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar alla subkategori som har subkategori id
$app->get('/api/supplier/subcategories/{cid}', function(Request $request, Response $response) {
    $cid = $request->getAttribute('cid');
    $sql = "SELECT * FROM supplier_subcategory where category_id = :cid";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':cid', $cid);
    $data = $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// skapar en kategori för supplier
$app->post('/api/supplier/categories/add', function(Request $request, Response $response) {
    ucfirst(strtolower(filter_var($name = $request->getParam('category_name'))));
    ucfirst(strtolower(filter_var($description = $request->getParam('description'))));
    intval($supplier_id = $request->getParam('supplier_id'));
    $sql = "INSERT INTO supplier_category (name, description, supplier_id) VALUES (:name, :description, :supplier_id)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':description', $description);
    $stmt->bindParam(':supplier_id', $supplier_id);
    $data = $stmt->execute();

    echo json_encode($data);
});

// skapar en subkategori för supplier
$app->post('/api/supplier/subcategories/add', function(Request $request, Response $response) {
    intval($supplier_id = $request->getParam('supplier_id'));
    ucfirst(strtolower(filter_var($sub_name = $request->getParam('sub_name'))));
    $sql = "INSERT INTO supplier_subcategory (sub_name, supplier_id) VALUES (:sub_name, :supplier_id)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sub_name', $sub_name);
    $stmt->bindParam(':supplier_id', $supplier_id);
    $data = $stmt->execute();

    echo json_encode($data);
});

// lägger till id från subkategori till kategori id
$app->put('/api/supplier/subcategories/{cid}/put', function(Request $request, Response $response) {
    intval($category_id = $request->getAttribute('cid'));
    intval($subcategory_id = $request->getParam('subcategory_id'));
    $sql = "UPDATE supplier_subcategory set category_id = :cid WHERE (subcategory_id = :subcategory_id)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':cid', $category_id);
    $stmt->bindParam(':subcategory_id', $subcategory_id);
    $data = $stmt->execute();

    echo json_encode($data);
});

// updaterar kategorier
$app->put('/api/supplier/categories/put', function(Request $request, Response $response) {
    intval($cid = $request->getParam('category_id'));
    ucfirst(strtolower(filter_var($name = $request->getParam('name'))));
    ucfirst(strtolower(filter_var($desc = $request->getParam('description'))));
    $sql = "UPDATE supplier_category SET name = :name, description = :desc WHERE (category_id = :cid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':cid', $cid);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':desc', $desc);
    $data = $stmt->execute();

    echo json_encode($data);
});

// ta bort kategori
$app->delete('/api/suppliers/categories/{cid}/delete', function(Request $request, Response $response) {
    $cid = $request->getAttribute('cid');
    $sql = "DELETE FROM supplier_category WHERE (category_id = $cid)";
    $stmt = DB::getConnection()->query($sql);
});

// tar bort subkategori
$app->put('/api/suppliers/subcategory/delete', function(Request $request, Response $response) {
    $cid = $request->getParam('subcategory_id');
    $sql = "UPDATE supplier_subcategory SET category_id = null WHERE (subcategory_id = $cid)";
    $stmt = DB::getConnection()->query($sql);
});

