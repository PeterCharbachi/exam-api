<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// hämtar alla startups/företag/ = supplier
$app->get('/api/suppliers', function(Request $request, Response $response) {
    $sql = "SELECT * FROM suppliers";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar specfik supplier
$app->get('/api/suppliers/{sid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM suppliers WHERE supplier_id = $sid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar usern som till här en supplier
$app->get('/api/supplier/{sid}/user', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM customers where supplier_id = $sid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar specfik kund
$app->get('/api/supplier/user/{cid}', function(Request $request, Response $response) {
    $cid = $request->getAttribute('cid');
    $sql = "SELECT * FROM customers where customer_id = $cid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// skapar kund för supplier
$app->post('/api/supplier/user/add', function(Request $request, Response $response) {
    intval($sid = $request->getParam('supplierId'));
    ucfirst(strtolower(filter_var($firstName = $request->getParam('firstName'))));
    ucfirst(strtolower(filter_var($lastName = $request->getParam('lastName'))));
    ucfirst(strtolower(filter_var( $address1 = $request->getParam('address1'))));
    ucfirst(strtolower(filter_var( $address2 = $request->getParam('address2'))));
    trim($phone = $request->getParam('phone'));
    strtolower(filter_var($email = $request->getParam('email')));
    $password = $request->getParam('password');
    $passwordHash = password_hash($password, PASSWORD_DEFAULT);


    $sql = "INSERT INTO customers (first_name, last_name, address1, address2, phone, email, password, supplier_id) VALUES (:firstName, :lastName, :address1, :address2, :phone, :email, :password, :sid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':firstName', $firstName);
    $stmt->bindParam(':lastName', $lastName);
    $stmt->bindParam(':address1', $address1);
    $stmt->bindParam(':address2',$address2);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $passwordHash);
    $data = $stmt->execute();

    echo json_encode($data);
});

// uppdaterar kund för supplier
$app->put('/api/supplier/user/put', function(Request $request, Response $response) {
    intval($cid = $request->getParam('customer_id'));
    ucfirst(strtolower(filter_var($firstName = $request->getParam('first_name'))));
    ucfirst(strtolower(filter_var($lastName = $request->getParam('last_name'))));
    ucfirst(strtolower(filter_var( $address1 = $request->getParam('address1'))));
    ucfirst(strtolower(filter_var( $address2 = $request->getParam('address2'))));
    trim($phone = $request->getParam('phone'));
    strtolower(filter_var($email = $request->getParam('email')));
    $password = $request->getParam('password');
    password_hash($password, PASSWORD_DEFAULT);

    $sql = "UPDATE customers SET first_name = :firstName, last_name = :lastName, address1 = :address1, address2 = :address2, phone = :phone, email = :email, password = :password WHERE (customer_id = :cid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':cid', $cid);
    $stmt->bindParam(':firstName', $firstName);
    $stmt->bindParam(':lastName', $lastName);
    $stmt->bindParam(':address1', $address1);
    $stmt->bindParam(':address2', $address2);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $password);
    $data = $stmt->execute();

    echo json_encode($data);
});

// tar bort kund för supplier
$app->delete('/api/supplier/user/{cid}/delete', function(Request $request, Response $response) {
    $cid = $request->getAttribute('cid');
    $sql = "DELETE FROM customers WHERE (customer_id = $cid)";
    $stmt = DB::getConnection()->query($sql);
});