<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// hämtar om oss för spefik supplier
$app->get('/api/supplier/homepage_aboutus/{sid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM homepage_aboutus where supplier_id = $sid";
    $stmt = DB::getConnection()->query($sql);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar navbar för spefik supplier
$app->get('/api/supplier/homepage/navbar/{sid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM homepage_navbar WHERE homePageN_sid = :sid";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $data = $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar spefik navbar som tillhör supplier
$app->get('/api/supplier/homepage/navbar/{sid}/{nav}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $navId = $request->getAttribute('nav');
    $sql = "SELECT * FROM homepage_navbar WHERE homePageN_sid = :sid and homePageN_id = :navId";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':navId', $navId);
    $data = $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar imgheader för specfik supplier
$app->get('/api/supplier/homepage/imgHeader/{sid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM homepage_imageheader WHERE homePageIB_sid = :sid";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $data = $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar alla imgbar som tillhör specfik 
$app->get('/api/supplier/homepage/imgBar/{sid}', function(Request $request, Response $response) {
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM homepage_imagebar WHERE homepageIB_sid = :sid";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $data = $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// hämtar specifik imgbar för specifik supplier
$app->get('/api/supplier/homepage/imgBar/{img}/{sid}', function(Request $request, Response $response) {
    $img = $request->getAttribute('img');
    $sid = $request->getAttribute('sid');
    $sql = "SELECT * FROM homepage_imagebar where homepageIB_id = :img and homepageIB_sid = :sid";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':img', $img);
    $data = $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($data);
});

// skapar navbar
$app->post('/api/supplier/homepage/navbar/add', function(Request $request, Response $response) {
    intval($supplier_id = $request->getParam('homePageN_sid'));
    ucfirst(strtolower(filter_var($navbarName = $request->getParam('homePageN_name'))));
    intval($category_id = $request->getParam('homePageN_category'));
    $sql = "INSERT INTO homepage_navbar (homePageN_sid, homePageN_name, homePageN_category) VALUES (:supplier_id, :navbarName, :category_id)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':supplier_id', $supplier_id);
    $stmt->bindParam(':navbarName', $navbarName);
    $stmt->bindParam(':category_id', $category_id);
    $data = $stmt->execute();

    echo json_encode($data);
});

// uppdaterar navbar
$app->put('/api/supplier/homepage/navbar/put', function(Request $request, Response $response) {
    intval($navId = $request->getParam('homePageN_id'));
    ucfirst(strtolower(filter_var($navbarName = $request->getParam('homePageN_name'))));
    intval($category_id = $request->getParam('homePageN_category'));
    $sql = "UPDATE homepage_navbar SET homePageN_name = :name, homePageN_category = :cid WHERE (homePageN_id = :navId)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':navId', $navId);
    $stmt->bindParam(':name', $navbarName);
    $stmt->bindParam(':cid', $category_id);
    $data = $stmt->execute();

    echo json_encode($data);
});

// skapar imgheader
$app->post('/api/supplier/homepage_supplier/imageheader/add', function(Request $request, Response $response) {
    intval($sid = $request->getParam('homePageSupplier'));
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 200000){
                $fileNameNew = uniqid(true). ".". $fileActualExt;
                $fileDestination = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\webbshop\src\assets\images/'. $fileNameNew;
                $fileDestination2 = 'C:\Users\Charbachi\Desktop\Angular\nytt\supplierAdmin\src\assets\images/'. $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                copy($fileDestination, $fileDestination2);
            } else{
                die("You're file is to big!");
            }
        }else{
            die("There was an error uploading your file!");
        }
    }else{
        die("You cannot upload files of this type!");
    }

    $sql = "INSERT INTO homepage_imageheader (homePageIB_Image, homePageIB_sid) VALUES (:imageHeader, :sid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':imageHeader', $fileNameNew);
    $data = $stmt->execute();

    echo json_encode($data);
});

// uppdaterar imgheader
$app->post('/api/supplier/homepage_supplier/imageheader/put', function(Request $request, Response $response) {
    intval($sid = $request->getParam('homePageSupplier'));
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 200000){
                $fileNameNew = uniqid(true). ".". $fileActualExt;
                $fileDestination = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\webbshop\src\assets\images/'. $fileNameNew;
                $fileDestination2 = 'C:\Users\Charbachi\Desktop\Angular\nytt\supplierAdmin\src\assets\images/'. $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                copy($fileDestination, $fileDestination2);
            } else{
                die("You're file is to big!");
            }
        }else{
            die("There was an error uploading your file!");
        }
    }else{
        die("You cannot upload files of this type!");
    }

    $sql = "UPDATE homepage_imageheader SET homePageIB_Image = :imageHeader WHERE (homePageIB_sid = :sid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':imageHeader', $fileNameNew);
    $data = $stmt->execute();

    echo json_encode($data);
});

// uppdaterar imgbar
$app->post('/api/supplier/homepage_imagebar/put', function(Request $request, Response $response) {
    intval($img = $request->getParam('img'));
    ucfirst(strtolower(filter_var($name = $request->getParam('name'))));
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 200000){
                $fileNameNew = uniqid(true). ".". $fileActualExt;
                $fileDestination = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\webbshop\src\assets\images/'. $fileNameNew;
                $fileDestination2 = 'C:\Users\Charbachi\Desktop\Angular\nytt\supplierAdmin\src\assets\images/'. $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                copy($fileDestination, $fileDestination2);
            } else{
                die("You're file is to big!");
            }
        }else{
            die("There was an error uploading your file!");
        }
    }else{
        die("You cannot upload files of this type!");
    }

    $sql = "UPDATE homepage_imagebar SET homepageIB_image = :imageBar, homepageIB_name = :name WHERE (`homepageIB_id` = :img)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':img', $img);
    $stmt->bindParam(':imageBar', $fileNameNew);
    $stmt->bindParam(':name', $name);
    $data = $stmt->execute();

    echo json_encode($data);
});

// skapar aboutus
$app->post('/api/supplier/homepage_aboutus/add', function(Request $request, Response $response) {
    intval($sid = $request->getParam('supplier_id'));
    ucfirst(strtolower(filter_var($address = $request->getParam('address'))));
    intval($zipCode = $request->getParam('zipCode'));
    ucfirst(strtolower(filter_var($city = $request->getParam('city'))));
    trim($number = $request->getParam('number'));
    strtolower(filter_var($email = $request->getParam('email')));
    ucfirst(strtolower(filter_var($textField = $request->getParam('textField'))));
    $sql = "INSERT INTO homepage_aboutus (address, zip_code, city, number, email, textField, supplier_id) VALUES (:address, :zipCode, :city, :number, :email, :textField, :sid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':zipCode', $zipCode);
    $stmt->bindParam(':city', $city);
    $stmt->bindParam(':number', $number);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':textField', $textField);
    $data = $stmt->execute();

    echo json_encode($data);
});

// skapar imgbar
$app->post('/api/supplier/homepage_imagebar/add', function(Request $request, Response $response) {
    intval($sid = $request->getParam('supplierId'));
    ucfirst(strtolower(filter_var($name = $request->getParam('name'))));
    intval($pid = $request->getParam('product_id'));
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 200000){
                $fileNameNew = uniqid(true). ".". $fileActualExt;
                $fileDestination = 'C:\Users\Charbachi\Desktop\Angular\examensarbete\webbshop\src\assets\images/'. $fileNameNew;
                $fileDestination2 = 'C:\Users\Charbachi\Desktop\Angular\nytt\supplierAdmin\src\assets\images/'. $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                copy($fileDestination, $fileDestination2);
            } else{
                die("You're file is to big!");
            }
        }else{
            die("There was an error uploading your file!");
        }
    }else{
        die("You cannot upload files of this type!");
    }

    $sql = "INSERT INTO homepage_imagebar (homepageIB_sid, homepageIB_image, homepageIB_name, homepageIB_product_id) VALUES (:sid, :imageBar, :name, :pid)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':sid', $sid);
    $stmt->bindParam(':imageBar', $fileNameNew);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':pid', $pid);
    $data = $stmt->execute();

    echo json_encode($data);
});

// uppdaterar about us
$app->put('/api/supplier/homepage_aboutus/put', function(Request $request, Response $response) {
    intval($id = $request->getParam('id'));
    ucfirst(strtolower(filter_var($address = $request->getParam('address'))));
    intval($zipCode = $request->getParam('zipCode'));
    ucfirst(strtolower(filter_var($city = $request->getParam('city'))));
    trim($number = $request->getParam('number'));
    strtolower(filter_var($email = $request->getParam('email')));
    ucfirst(strtolower(filter_var($textField = $request->getParam('textField'))));

    $sql = "UPDATE homepage_aboutus SET address = :address, zip_code = :zipCode, city = :city, number = :number, email = :email, textField = :textField WHERE (id = :id)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':zipCode', $zipCode);
    $stmt->bindParam(':city', $city);
    $stmt->bindParam(':number', $number);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':textField', $textField);
    $data = $stmt->execute();
});

// tar bort specifik navbar
$app->delete('/api/supplier/homepage/navbar/{nav}/delete', function(Request $request, Response $response) {
    $nav = $request->getAttribute('nav');
    $sql = "DELETE FROM homepage_navbar WHERE (homePageN_id = $nav)";
    $stmt = DB::getConnection()->query($sql);
});

// tar bort imgbar
$app->delete('/api/supplier/homepage_imagebar/{img}/delete', function(Request $request, Response $response) {
    $img = $request->getAttribute('img');
    $sql = "DELETE FROM homepage_imagebar WHERE (homepageIB_id = $img)";
    $stmt = DB::getConnection()->query($sql);
});
