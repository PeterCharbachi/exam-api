<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// kund login
$app->post('/api/customer/login', function(Request $request, Response $response) {

    strtolower(filter_var($username = $request->getParam('username')));
    $password = $request->getParam('password');
    $sql = "SELECT * FROM customers WHERE email = :username";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':username', $username);
    // $stmt->bindParam(':password', $password);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    // sparar lösenordet i en variabel 
    $hashedPwd = $data['password'];

    // kollar om lösenordet passar med det som kom från input
    if (password_verify($password, $hashedPwd)) {

    } else {
        // Om användarnamn eller lösenord är fel, se till att objektet är utloggat.
        die('die');
    }


    if($data == false){
        die('wrong user');
    }

    
    // UPDATE products SET name = :name, description = :description, price = :price, vat = :vat, stock = :stock, active = :active WHERE pid=:pid
    // $secret = $request->getParam('secret');
    // skapar en secret som läggs in i databasen med andra ord token typ
    function unique_id($l = 8) {
        return substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(32))), 0, 32); // 32 chars, without /=+
    }

    // och sparar det i secret och i databasen
    $secret = uniqid();
    $id = $data['customer_id'];
    $sql = "UPDATE customers SET secret = :secret WHERE customer_id = :id";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':secret', $secret);
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    $test = array('secret' => $secret, 'user' => $data['customer_id'], 'first_name' => $data['first_name']);
    
   

    header('Content-Type: application/json');
    echo json_encode($test);
    
        
    
});

// secret skickas in och hämtar kundens data 
$app->get('/api/customer/login/{secret}', function(Request $request, Response $response) {
    $secret = $request->getAttribute('secret');
    $sql = "SELECT * FROM customers WHERE secret = :secret";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':secret', $secret);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    header('Content-Type: application/json');
    echo json_encode($data);

    // if($data == false) {
    //     echo json_encode($data);
    // } else {
    //     echo json_decode(true);
    // }

});

// samma gäller för suppliers login
$app->post('/api/supplier/login', function(Request $request, Response $response) {
    // and `password` = :password 
    strtolower(filter_var($username = $request->getParam('username')));
    $password = $request->getParam('password');
    // $sql = "SELECT password FROM customers WHERE email = :username AND supplier_id IS NOT NULL";
    $sql = "SELECT * FROM customers WHERE email = :username AND supplier_id IS NOT NULL";

    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':username', $username);
    // $stmt->bindParam(':password', $password);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    $hashedPwd = $data['password'];


    if (password_verify($password, $hashedPwd)) {

    } else {
        // Om användarnamn eller lösenord är fel, se till att objektet är utloggat.
        die('die');
    }


    if($data == false){
        die('wrong user');
    }

    // skapar en funktion som generar random sifror 
    function unique_id($l = 8) {
        return substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(32))), 0, 32); // 32 chars, without /=+
    }

    // och sparar det i secret och i databasen
    $secret = uniqid();
    $id = $data['customer_id'];
    $sql = "UPDATE customers SET secret = :secret WHERE customer_id = :id";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':secret', $secret);
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    $test = array('secret' => $secret, 'user' => $data['supplier_id']);



    header('Content-Type: application/json');
    echo json_encode($test);



});
// secret skickas in och supplier får datan 
$app->get('/api/suppliers/login/{secret}', function(Request $request, Response $response) {
    $secret = $request->getAttribute('secret');
    $sql = "SELECT * FROM suppliers,customers WHERE secret = :secret and customers.supplier_id = suppliers.supplier_id";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':secret', $secret);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    header('Content-Type: application/json');
    echo json_encode($data);

    // if($data == false) {
    //     echo json_encode($data);
    // } else {
    //     echo json_decode(true);
    // }

});
// skapar supplier och man blir även en customer på webshopen
$app->post('/api/supplier/signup/add', function(Request $request, Response $response) {
    ucfirst(strtolower(filter_var($companyName = $request->getParam('companyName'))));
    ucfirst(strtolower(filter_var($contactFirstName = $request->getParam('contactFirstName'))));
    ucfirst(strtolower(filter_var($contactLastName = $request->getParam('contactLastName'))));
    ucfirst(strtolower(filter_var($supplierAddress1 = $request->getParam('supplierAddress1'))));
    ucfirst(strtolower(filter_var($supplierAddress2 = $request->getParam('supplierAddress2'))));
    trim($supplierPhone = $request->getParam('supplierPhone'));
    strtolower(filter_var($supplierEmail = $request->getParam('supplierEmail')));


    $sql = "INSERT INTO suppliers (company_name, contact_first_name, contact_last_name, supplier_address1, supplier_address2, supplier_phone, supplier_email) VALUES (:company_name, :contact_first_name, :contact_last_name, :supplier_address1, :supplier_address2, :supplier_phone, :supplier_email)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':company_name', $companyName);
    $stmt->bindParam(':contact_first_name', $contactFirstName);
    $stmt->bindParam(':contact_last_name', $contactLastName);
    $stmt->bindParam(':supplier_address1', $supplierAddress1);
    $stmt->bindParam(':supplier_address2', $supplierAddress2);
    $stmt->bindParam(':supplier_phone', $supplierPhone);
    $stmt->bindParam(':supplier_email', $supplierEmail);
    $data = $stmt->execute();


    $newCartId = DB::getConnection()->lastInsertId();


    ucfirst(strtolower(filter_var($firstName = $request->getParam('firstName'))));
    ucfirst(strtolower(filter_var($lastName = $request->getParam('lastName'))));
    ucfirst(strtolower(filter_var($address1 = $request->getParam('address1'))));
    ucfirst(strtolower(filter_var($address2 = $request->getParam('address2'))));
    trim($phone = $request->getParam('phone'));
    strtolower(filter_var($email = $request->getParam('email')));
    $password = $request->getParam('password');
    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
    // password_hash($password), PASSWORD_DEFAULT);
    $sql = "INSERT INTO customers (first_name, last_name, address1, address2, phone, email, password, supplier_id) VALUES (:first_name, :last_name, :address1, :address2, :phone, :email, :password, :supplier_id )";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':first_name', $firstName);
    $stmt->bindParam(':last_name', $lastName);
    $stmt->bindParam(':address1', $address1);
    $stmt->bindParam(':address2', $address2);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $passwordHash);
    $stmt->bindParam(':supplier_id', $newCartId);
    $data = $stmt->execute();

    header('Content-Type: application/json');

    echo json_encode($data);
});

// skapar en customer
$app->post('/api/customer/signup/add', function(Request $request, Response $response) {

    ucfirst(strtolower(filter_var($firstName = $request->getParam('firstName'))));
    ucfirst(strtolower(filter_var($lastName = $request->getParam('lastName'))));
    ucfirst(strtolower(filter_var($address1 = $request->getParam('address1'))));
    ucfirst(strtolower(filter_var($address2 = $request->getParam('address2'))));
    trim($phone = $request->getParam('phone'));
    strtolower(filter_var($email = $request->getParam('email')));
    $password = $request->getParam('password');
    $passwordHash = password_hash($password, PASSWORD_DEFAULT);

    // password_hash($password), PASSWORD_DEFAULT);
    $sql = "INSERT INTO customers (first_name, last_name, address1, address2, phone, email, password) VALUES (:first_name, :last_name, :address1, :address2, :phone, :email, :password)";
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindParam(':first_name', $firstName);
    $stmt->bindParam(':last_name', $lastName);
    $stmt->bindParam(':address1', $address1);
    $stmt->bindParam(':address2', $address2);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $passwordHash);
    $data = $stmt->execute();

    header('Content-Type: application/json');

    echo json_encode($data);
});