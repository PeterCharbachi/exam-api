<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-HTTP-Method-Override, Origin, X-Requested-With, Content-Type, Accept, Authorization");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header('Content-Type: application/json');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App();

// url http://localhost/project/src/public/api/{filnamnet}

// när man lägget till en bild på en produkt eller imgheader mfl har jag valt att bilden får max vara 200kb och skapat en array som säger vilka format en bild få ha.


// includerar alla mina apier
require ('../api/suppliers.php');
require ('../api/homePage.php');
require ('../api/orders.php');
require ('../api/category.php');
require ('../api/customer.php');
require ('../api/product.php');
require ('../api/signUp.php');
require ('../api/DB.php');


$app->run();
